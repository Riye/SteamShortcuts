import sys,re,os,winshell,glob,string
from win32com.client import Dispatch

def findLibrarys(base_steam_dir):
	filePath = os.path.join(base_steam_dir,"steamapps","libraryfolders.vdf")
	file = open(filePath,"r").read()
	return re.findall(r'"[0-9]*"\t*"(.*)"',file)

def findAppManifests(library):
	searchPath = os.path.join(library,"steamapps","appmanifest_*.acf")
	return glob.glob(searchPath)

def parseManifest(path):
	file = open(path,"r").read()
	return dict(re.findall(r'"(.*)"\t*"(.*)"',file))

def findIcon(path):

	files = glob.glob(os.path.join(path,"*.exe"))
	largest = sorted((os.path.getsize(s),s) for s in files)[-1][1] if files else ''
	if largest != '':
		return largest
	else:
		files = glob.glob(os.path.join(path,"**","*.exe"),recursive=True)
		largest = sorted((os.path.getsize(s),s) for s in files)[-1][1] if files else ''
		if largest:
			return largest+",0"
		else:
			return ''

def createShortcut(path, target='', icon=''):    
    shell = Dispatch('WScript.Shell')
    shortcut = shell.CreateShortCut(path)
    shortcut.Targetpath = target
    if icon == '':
        pass
    else:
        shortcut.IconLocation = icon
    shortcut.save()




output_dir = os.path.join(winshell.desktop(),"Launchy","Games")
base_steam_dir = "C:\\Program Files (x86)\\Steam"

librarys = findLibrarys(base_steam_dir)
librarys.append(os.path.join(base_steam_dir))
for lib in librarys:
	sys.stdout.write("Searching for Games in " + lib + "\n")
	sys.stdout.flush()
	app_manifests = findAppManifests(lib)
	sys.stdout.write("Found " + str(len(app_manifests)) + " games" + "\n")
	sys.stdout.flush()
	for app in app_manifests:
		data = parseManifest(app)
		if 'appid' in data:
			sys.stdout.write("\t Adding " + data['name'] + "\n")
			sys.stdout.flush()
			target = "steam://rungameid/" + data['appid']
			icon = findIcon(os.path.join(lib,"steamapps","common",data['installdir']))
			path = os.path.join(output_dir,re.sub('[\\/:*?<>|]', '', data['name']))+".lnk"
			createShortcut(path,target,icon)